from fastapi import APIRouter, FastAPI
from typing import List
from turtle import st
from pydantic import BaseModel
from passlib.context import CryptContext

from DATABASE.db_conn import engineconn
from DATABASE.db_class import User_Data
from DATABASE import class_user


app = FastAPI()

engine = engineconn()
session = engine.sessionmaker()

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def get_user(db: session, username: str):
    return db.query(User_Data).filter(User_Data.user_id == username).first()

def get_existing_user(db: session, user_create: class_user.user_join):
    return db.query(User_Data).filter(
        (User_Data.username == user_create.username) |
        (User_Data.user_id == user_create.user_id)
    ).first()

def create_user(db: session, user : class_user.user_join):
    db_user = User_Data(username=user.user_id,
                   password=pwd_context.hash(user.user_password),
                   email=user.user_email)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

def update_user(db: session, db_user: User_Data, user_update : class_user.user_update):
    
    return db.query(User_Data).filter(User_Data.user_id == username).first()

def delete_user(db: User_Data, username: str):
    return db.query(User_Data).filter(User_Data.user_id == username).first()