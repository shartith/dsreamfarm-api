from turtle import st
from pydantic import BaseModel
from typing import Optional

class role(BaseModel):
    user_id: str
    user_role: str
    user_role_status: int