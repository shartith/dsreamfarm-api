from sqlalchemy import Column, DATETIME, VARCHAR, TEXT, INT, SMALLINT, FLOAT, BIGINT
from sqlalchemy.orm import declarative_base

Base = declarative_base()

class Console_Data(Base):
    __tablename__ = 'console_data'
    console_id = Column(VARCHAR, nullable=False, primary_key=True)
    console_update = Column(DATETIME, nullable=False)
    console_status = Column(SMALLINT, nullable=False)

class Machine_Data(Base):
    __tablename__ = 'machine_data'
    machine_id = Column(VARCHAR, nullable=False, primary_key=True)
    machine_name = Column(VARCHAR, nullable=False)
    machine_type = Column(VARCHAR, nullable=False)
    machine_lat = Column(FLOAT, nullable=True)
    machine_lon = Column(FLOAT, nullable=True)
    machine_addr = Column(VARCHAR, nullable=True)
    machine_status = Column(SMALLINT, nullable=False)
    machine_pairing = Column(VARCHAR, nullable=True)

class Reg_Data(Base):
    __tablename__ = 'reg_data'
    reg_id = Column(BIGINT, nullable=False, autoincrement=True, primary_key=True)
    reg_console_id = Column(BIGINT, nullable=False)
    reg_date = Column(DATETIME, nullable=False)
    reg_temperature = Column(FLOAT, nullable=True)
    reg_humidity = Column(FLOAT, nullable=True)
    reg_co2 = Column(FLOAT, nullable=True)
    reg_lux = Column(INT, nullable=True)
    reg_ph = Column(FLOAT, nullable=True)
    reg_ec = Column(FLOAT, nullable=True)

class Token_Data(Base):
    __tablename__ = 'token_data'
    token_id = Column(VARCHAR, nullable=False, primary_key=True)
    token_user = Column(VARCHAR, nullable=False,)
    token_regdate = Column(DATETIME, nullable=False)
    token_expire = Column(DATETIME, nullable=False)

class User_Data(Base):
    __tablename__ = 'user_data'
    user_id = Column(VARCHAR, nullable=False, primary_key=True)       
    user_name = Column(VARCHAR, nullable=False,)    
    user_email = Column(VARCHAR, nullable=False,)   
    user_phone = Column(VARCHAR, nullable=True,)   
    user_password = Column(VARCHAR, nullable=False,)
    user_status = Column(SMALLINT, nullable=False)