from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

app = {
    "name": "mysql+pymysql",
    "user": "dreamfarm",
    "password": "emfla123",
    "host": "localhost",
    "port": 3306,
    "database": "dreamfarm",
    "char": "utf8"
}

conn_string = f"{app['name']}://{app['user']}:{app['password']}@{app['host']}:{app['port']}/{app['database']}?charset={app['char']}&connect_timeout=10&autocommit=True"

class engineconn:

    def __init__(self):
        self.engine = create_engine(conn_string, pool_recycle=500)

    def sessionmaker(self):
        Session = sessionmaker(bind=self.engine)
        session = Session()
        return session

    def connection(self):
        conn = self.engine.connect()
        return conn