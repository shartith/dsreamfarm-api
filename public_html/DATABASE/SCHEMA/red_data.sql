-- auto-generated definition
create table reg_data
(
    reg_id          bigint auto_increment comment '센서데이터 고유아이디'
        primary key,
    reg_console_id  varchar(12)  not null comment '센서데이터 송신 콘솔아이디',
    reg_date        datetime     not null comment '센서데이터 송신 시간', --수신시간 아님
    reg_temperature float(23, 0) null comment '온도 센서 데이터',
    reg_humidity    float(23, 0) null comment '습도 센서 데이터',
    reg_co2         float(23, 0) null comment '이산화탄소 센서 데이터',
    reg_lux         int          null comment '조도 센서 데이터',
    reg_ph          float(23, 0) null comment '산도 센서 데이터',
    reg_ec          float(23, 0) null comment '전해질 센서 데이터'
)
comment '데이터정보';

create index reg_console_id
    on reg_data (reg_console_id);