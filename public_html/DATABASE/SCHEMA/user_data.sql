-- auto-generated definition
create table user_data
(
    user_id       varchar(20)        not null comment '사용자아이디'
        primary key,
    user_name     varchar(20)        not null comment '사용자이름',
    user_email    varchar(100)       not null comment '사용자이메일',
    user_phone    varchar(13)        null comment '사용자연락처',
    user_password varchar(255)       not null comment '사용자패스워드',
    user_status   smallint default 1 not null comment '사용자상태' -- 0:denied / 1:agree
)
comment '사용자정보';