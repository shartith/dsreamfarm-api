-- auto-generated definition
create table token_data
(
    token_id      varchar(255) not null comment '발급토근키';
        primary key,
    token_user    varchar(20)  not null comment '발급자';,
    token_regdate datetime     not null comment '최초발급일자';,
    token_expire  datetime     not null comment '발급종료일자';
)
comment '토큰정보';