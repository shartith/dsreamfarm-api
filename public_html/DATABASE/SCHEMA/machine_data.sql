-- auto-generated definition
create table machine_data
(
    machine_id      bigint                not null comment '스마트팜 고유아이디'
        primary key,
    machine_name    varchar(255)          not null comment '스마트팜 명칭',
    machine_type    varchar(255)          not null comment '스마트팜 종류',
    machine_lat     float                 null comment '스마트팜 위도',
    machine_log     float                 null comment '스마트팜 경도',
    machine_addr    varchar(255)          null comment '스마트팜 설치 주소',
    machine_status  smallint(1) default 1 not null comment '스마트팜 승인 상태',
    machine_pairing varchar(12)           null comment '연결된 콘솔장치'
)
comment '스마트팜 정보';