-- auto-generated definition
create table console_data
(
    console_id     varchar(12)           not null comment '콘솔장치 맥어드레스'
        primary key,
    console_update datetime              not null comment '콘솔장치 등록일자',
    console_status smallint(1) default 1 not null comment '콘솔장치 사용유무',
    console_expire datetime              null comment '콘솔만료기간'
)
comment '콘솔정보';