-- auto-generated definition
create table user_role
(
    user_id          varchar(20) not null comment '사용자 아이디',
    user_role        varchar(50) not null comment '사용자 권한',
    user_role_status smallint(1) not null comment '사용자 권한 승인유무' -- 0:denied / 1:agree
)
comment '사용자권한';


-- user role

-- JOIN_USER
-- UPDATE_USER
-- DELETE_USER

-- LIST_CONSOLE