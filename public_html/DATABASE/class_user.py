from pydantic import BaseModel
from typing import Optional


class User_join(BaseModel):
    user_token: Optional[str]=None
    user_id: str
    user_password: str
    user_name: str
    user_email: str
    user_phone: str
    user_postcode: Optional[str]=None
    user_address: Optional[str]=None
    user_detailAddress: Optional[str]=None
    user_role: Optional[str]=None

class User_update(BaseModel):
    user_id: str
    user_password: Optional[str]=None
    user_name: str
    user_email: str
    user_phone: str
    user_postcode: Optional[str]=None
    user_address: Optional[str]=None
    user_detailAddress: Optional[str]=None
    user_role: Optional[str]=None

class User_delete(BaseModel):
    User_token: Optional[str]=None
    user_id: str

class User_login(BaseModel):
  user_id: str
  user_password: str

class User_token(BaseModel):
  user_id: str

class user_password(BaseModel):
    user_id: str
    user_oldpw: str
    user_newpw: str

class user_data(BaseModel):
   user_id:str