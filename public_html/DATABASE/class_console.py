from datetime import date, datetime
from turtle import st
from pydantic import BaseModel
from typing import Optional

class update_console(BaseModel):
    token: str
    console_id: str
    console_status: Optional[int] = None
    console_expire: Optional[datetime] = None

class delete_console(BaseModel):
    token: str
    console_id: str