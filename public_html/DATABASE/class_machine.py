from datetime import date, datetime
from turtle import st
from pydantic import BaseModel
from typing import Optional

class machine(BaseModel):
    machine_id: str
    machine_name: str
    machine_type: str
    machine_status: int
    machine_lat: Optional[float] = None
    machine_lan: Optional[float] = None
    machine_addr: Optional[str] = None
    machine_paring: Optional[str] = None