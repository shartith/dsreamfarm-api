from fastapi import APIRouter, FastAPI
from fastapi.responses import StreamingResponse,FileResponse
from pydantic import BaseModel
from typing import Optional

from DATABASE.db_conn import engineconn

import datetime
import pandas as pd
from io import BytesIO
import tempfile

router = APIRouter(
    prefix="/controller",  # url 앞에 고정적으로 붙는 경로추가
    tags=["controller"]
)  # Route 분리

app = FastAPI()

engine = engineconn()
session = engine.sessionmaker()

class Controller_sensor_data_graph(BaseModel):
   controller_id: str
   sensor_type: str
   
class Controller_create(BaseModel):
    controller_id: str
    controller_update: Optional[str]=None 
    controller_status: Optional[str]=None
    controller_expire: Optional[str]=None
    controller_auto_status: Optional[str]=None

class Controller_detail(BaseModel):
    controller_id: str

class Controller_change_auto_status(BaseModel):
    controller_id: str
    controller_auto_status: str

class Controller_sensor_data(BaseModel):
    controller_id: str
    sensor_type: str
    sensor_date: str

class Controller_sensor_data_user(BaseModel):
    controller_id: str
    sensor_type: str
    show_type: str


@router.post("/api/sensor/info_sensor_data")
async def info_sensor_data(sensor_id: str):
    lists = session.execute(f"call info_sensor_data('{sensor_id}')").all()
    return lists
          
@router.post("/create")
async def controller_create(controller:Controller_create):
  session.execute(f"call Controller_create('{controller.controller_id}')")

@router.get("/read")
async def controller_read():
  controller_infos = session.execute(f"call controller_read()").all()
  return controller_infos

@router.post("/detail")
async def controller_read(controller:Controller_detail):
  controller_info = session.execute(f"call controller_detail('{controller.controller_id}')").all()
  return controller_info

@router.get("/list")
async def controller_list():
  session.execute(f"call controller_list()")

@router.get("/list_nopage")
async def controller_list_nopage():
  controller_list = session.execute(f"call controller_list_nopage()").all()
  return controller_list

@router.post("/update")
async def controller_update(controller:Controller_create):
  session.execute(f"call controller_update('{controller.controller_id}')")

@router.post("/delete")
async def controller_delete(controller:Controller_create):
  session.execute(f"call controller_delete('{controller.controller_id}')")

@router.post("/change_auto_status")
async def controller_change_auto_status(controller:Controller_change_auto_status):
  controller_result = session.execute(f"call controller_change_auto_status('{controller.controller_id}','{controller.controller_auto_status}')").all()
  return controller_result

@router.post("/sensor_data")
async def sensor_data(controller:Controller_sensor_data):
  sensor_data = session.execute(f"call sensor_data('{controller.controller_id}','{controller.sensor_type}','{controller.sensor_date}')").all()
  return sensor_data

@router.post("/sensor_data_app")
async def sensor_data_app(controller:Controller_sensor_data_user):
  sensor_data = session.execute(f"call sensor_data_user('{controller.controller_id}','{controller.sensor_type}','{controller.show_type}')").all()
  return sensor_data

@router.post("/sensor_graph")
async def sensor_data_app_graph(controller:Controller_sensor_data_graph):
   try:
    sensor_data = session.execute(f"call sensor_graph('{controller.controller_id}','{controller.sensor_type}')").all()
    return sensor_data
   except Exception as e:
        return {"error": str(e)}

@router.post("/sensor_temp")
async def sensor_temp(controller: Controller_detail):
    try:
        current_datetime = datetime.datetime.now()
        previous_datetime = current_datetime - datetime.timedelta(hours=24)
        previous_datetime_str = previous_datetime.strftime('%Y-%m-%d %H:%M')
        previous_datetime_end = datetime.datetime.now()
        controller_ids = controller.controller_id.split(",")  # 콤마로 문자열을 분리합니다.
        controller_ids = [f"'{id.strip()}'" for id in controller_ids]  # 각 문자열에 작은따옴표를 추가하고 양쪽의 공백을 제거합니다.

        # ' OR reg_controller_id=' 연산자를 사용하여 컨트롤러 ID를 연결합니다.
        controller_condition = " OR reg_controller_id=".join(controller_ids)
        controller_condition = f"{controller_condition}"  # 추가: 컨트롤러 조건을 작은따옴표로 둘러싸줍니다.

        query = f"SELECT reg_controller_id, reg_temperature, regDate FROM (\
    SELECT reg_controller_id, reg_temperature, LEFT(reg_date, 16) AS regDate \
    FROM reg_data \
    WHERE MINUTE(reg_date) % 5 = 0 \
    AND reg_controller_id = {controller_condition} \
    AND LEFT(reg_date, 16) BETWEEN '{previous_datetime_str}' AND '{previous_datetime_end}' \
    ORDER BY reg_date DESC) AS sub WHERE MINUTE(regDate) % 5 = 0 \
    AND regDate BETWEEN '{previous_datetime_str}' AND '{previous_datetime_end}' \
    ORDER BY regDate ASC"

        sensor_data = session.execute(query).all()  # 결과를 가져오기 위해 `all()`을 호출합니다.
        return sensor_data
    except Exception as e:
        return {"error": str(e)}
    
@router.post("/senser_data_download_excel")
async def senser_data_download_excel(controller: Controller_detail):
    try:
        sql = f"SELECT *\
              FROM reg_data\
              WHERE reg_controller_id = '{controller.controller_id}'\
                AND LEFT(reg_date, 16) BETWEEN '2023-06-19 15:00' AND '2023-07-27 15:00'\
                AND reg_temperature > 0\
                AND reg_humidity > 0\
                AND reg_co2 > 0\
                AND reg_lux > 0\
                AND reg_ph > 0\
                AND reg_ec > 0\
                AND reg_etemp > 0\
                AND reg_ehumi > 0\
              ORDER BY reg_date ASC"
              
        result = session.execute(sql).all()

        # Convert the result into a pandas DataFrame
        df = pd.DataFrame(result)

        # Create a temporary file to save the Excel data
        with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
            tmp_filename = tmp_file.name
            df.to_excel(tmp_file.name, engine='openpyxl', index=False)

        # Return the Excel file using FileResponse
        return FileResponse(tmp_filename, filename="sensor_data.xlsx")

    except Exception as e:
        return {"error": str(e)}