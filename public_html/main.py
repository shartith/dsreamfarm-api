from fastapi import FastAPI, BackgroundTasks
from user import router as user_router
from controller import router as controller_router
from machine import router as machine_router
from unit import router as unit_router
from sms import router as sms_router
from update import router as update_router

from fastapi.middleware.cors import CORSMiddleware
import uvicorn
import asyncio

app = FastAPI()
origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Include all the routers
app.include_router(user_router)
app.include_router(controller_router)
app.include_router(machine_router)
app.include_router(unit_router)
app.include_router(sms_router)
app.include_router(update_router)

# Function to run the SMS API in the background
async def run_sms_api():
    from DATABASE.db_conn import engineconn
    from time import sleep
    from requests import post
    
    engine = engineconn()
    session = engine.sessionmaker()
    
    mass_send_url = 'https://apis.aligo.in/send_mass/'
    
    while True:
        result = session.execute("CALL sms_data()").all()
        if len(result) >= 1:
            sms_data = {
                'key': '8f6absa8gmm2nvad8caroxgeipfbemq9',  # api key
                'userid': 'asj0352',  # 알리고 사이트 아이디
                'sender': '01034841507',  # 발신번호
                'cnt': len(result),  # 메세지 전송건수(번호, 메세지 매칭건수)
                'msg_type': 'SMS',  # 메세지 타입 (SMS, LMS, MMS)
                'testmode_yn': 'N'  # 테스트모드 적용 여부 Y/N
            }
            delete_data = []  # 삭제할 데이터를 저장할 리스트

            for i, row in enumerate(result):
                sms_data[f'rec_{i+1}'] = row.unit_tel
                sms_data[f'msg_{i+1}'] = row.unit_sms
                delete_data.append({'unit_tel': row.unit_tel, 'unit_sms': row.unit_sms})  # 삭제할 데이터 추가

            post(mass_send_url, data=sms_data)
            delete_sms_data(delete_data)  # 삭제할 데이터를 프로시저로 전달하여 삭제

        await asyncio.sleep(10)  # 10초 대기

def delete_sms_data(data):
    from DATABASE.db_conn import engineconn
    engine = engineconn()
    session = engine.sessionmaker()
    
    for item in data:
        unit_tel = item['unit_tel']
        unit_sms = item['unit_sms']
        session.execute("CALL sms_delete(:unit_tel, :unit_sms)", {'unit_tel': unit_tel, 'unit_sms': unit_sms})
    session.commit()

async def run_danger_message():
    from DATABASE.db_conn import engineconn
    engine = engineconn()
    session = engine.sessionmaker()

    while True:
        session.execute("CALL danger_message()")
        session.commit()
        await asyncio.sleep(120)  # 1분 대기
# Background task to run the SMS API

@app.on_event("startup")
async def startup_event():
    asyncio.create_task(run_sms_api())
    asyncio.create_task(run_danger_message())