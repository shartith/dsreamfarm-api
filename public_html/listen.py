import socket
from DATABASE.db_conn import engineconn

# 데이터베이스 연결
engine = engineconn()
session = engine.sessionmaker()

# 서버연결 스레드

def listen_message(client_socket, addr):
    while True:
        try:
            # data에 소켓정보가 들어올때까지 기다림
            data = client_socket.recv(1024) 

            if not data:
                print('>> Disconnect From Client : ', addr[0], ' / ', addr[1])
                break
            
            macaddr = data.decode()
            #lists = []
            #lists = session.execute(f"call info_unit_data('{macaddr}')").all()
            #lists['unit_time'] = session.execute(f"call info_unit_timer('{macaddr}')").all()
            #lists['sensor_data'] = session.execute(f"call info_sensor_data('{macaddr}')").all()
            global client_sockets
            print(client_sockets)
            
            #start_new_thread(listen_message, (client_socket, data.decode()))

        except ConnectionResetError as e:
            print('>> Disconnect Form Server: ' + addr[0], ' / ', addr[1])
            break

    # 위 while이 종료되면 클라이언트 목록에서 삭제
    if client_socket in client_sockets :
        client_sockets.remove(client_socket)
        print('>> Connected Count : ', len(client_sockets))

    client_socket.close()

def listen_database(client_socket, macaddr):
    while True:
        client_socket.send()