import os

from fastapi import APIRouter, FastAPI, UploadFile
from fastapi.responses import FileResponse
from typing import Optional

router = APIRouter(
    prefix="/update",  # url 앞에 고정적으로 붙는 경로추가
    tags=["update"]
)  # Route 분리

@router.post("/upload_main",summary="메인 업데이트")
async def upload_main(version:str,files:UploadFile):
    path = f"data/{version}"
    filepath = f"{path}/dreamfarm_main.ino.bin"

    if not os.path.exists(path):
        os.makedirs(path)
    
    try:
        with open(filepath, "wb+") as file_object:
            file_object.write(files.file.read())
    except:
        return {"status": "error", "msg": "파일 업로드 실패"}

    return None

@router.post("/upload_switch",summary="스위치보드 업데이트")
async def upload_main(version:str,files:UploadFile):
    path = f"data/{version}"
    filepath = f"{path}/dreamfarm_switchboard.ino.esp32s3.bin"

    if not os.path.exists(path):
        os.makedirs(path)
    
    try:
        with open(filepath, "wb+") as file_object:
            file_object.write(files.file.read())
    except:
        return {"status": "error", "msg": "파일 업로드 실패"}

    return None

@router.post("/upload_sensor",summary="센서보드 업데이트")
async def upload_main(version:str,files:UploadFile):
    path = f"data/{version}"
    filepath = f"{path}/dreamfarm_sensor.ino.bin"

    if not os.path.exists(path):
        os.makedirs(path)
    
    try:
        with open(filepath, "wb+") as file_object:
            file_object.write(files.file.read())
    except:
        return {"status": "error", "msg": "파일 업로드 실패"}

    return None

@router.post("/upload_er10x",summary="메인 업데이트")
async def upload_main(version:str,files:UploadFile):
    path = f"data/{version}"
    filepath = f"{path}/dreamfarm_er10x.ino.bin"

    if not os.path.exists(path):
        os.makedirs(path)
    
    try:
        with open(filepath, "wb+") as file_object:
            file_object.write(files.file.read())
    except:
        return {"status": "error", "msg": "파일 업로드 실패"}

    return None


@router.get("/download_main/{version}",summary="메인보드 업데이트 다운로드")
async def get_images(version:str):
    path = f"data/{version}"
    filepath = f"{path}/dreamfarm_main.ino.bin"

    if os.path.exists(filepath):
        return FileResponse(filepath, filename="dreamfarm_main.ino.bin", media_type="application/octet-stream")
    else:
        return False

@router.get("/download_switch/{version}",summary="스위치보드 업데이트 다운로드")
async def get_images(version:str):
    path = f"data/{version}"
    filepath = f"{path}/dreamfarm_switchboard.ino.esp32s3.bin"

    if os.path.exists(filepath):
        return FileResponse(filepath, filename="dreamfarm_switchboard.ino.esp32s3.bin", media_type="application/octet-stream")
    else:
        return False

@router.get("/download_sensor/{version}",summary="센서보드 업데이트 다운로드")
async def get_images(version:str):
    path = f"data/{version}"
    filepath = f"{path}/dreamfarm_sensor.ino.bin"

    if os.path.exists(filepath):
        return FileResponse(filepath, filename="dreamfarm_sensor.ino.bin", media_type="application/octet-stream")
    else:
        return False
    
@router.get("/download_er10x/{version}",summary="메인보드 업데이트 다운로드")
async def get_images(version:str):
    path = f"data/{version}"
    filepath = f"{path}/dreamfarm_er10x.ino.bin"

    if os.path.exists(filepath):
        return FileResponse(filepath, filename="dreamfarm_er10x.ino.bin", media_type="application/octet-stream")
    else:
        return False