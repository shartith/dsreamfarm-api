from fastapi import APIRouter, FastAPI
from typing import Optional

from DATABASE.db_conn import engineconn
from DATABASE import class_user

router = APIRouter(
    prefix="/user",  # url 앞에 고정적으로 붙는 경로추가
    tags=["user"]
)  # Route 분리

app = FastAPI()

engine = engineconn()
session = engine.sessionmaker()


@router.post("/login")
def user_login(user: class_user.User_login):
    token = session.execute(f"call user_login('{user.user_id}','{user.user_password}')").all()
    return token

@router.post("/web_login")
def user_login(user: class_user.User_login):
    token = session.execute(f"call user_web_login('{user.user_id}','{user.user_password}')").all()
    return token 

@router.post("/join") #create
def user_join(user: class_user.User_join):
    session.execute(f"call user_join('{user.user_token}','{user.user_id}','{user.user_name}','{user.user_email}','{user.user_password}','{user.user_phone}','{user.user_postcode}','{user.user_address}','{user.user_detailAddress}','{user.user_role}')")

@router.post("/list") #read
def user_list(token:str, rownum: Optional[int]=20, page: Optional[int]=1, cpage: Optional[int]=1, sdx: Optional[str]=None, sort: Optional[str]=None, keystr: Optional[str]=None, valstr: Optional[str]=None):
    session.execute(f"call user_list('{token}','{rownum}','{page}','{cpage}','{sdx}','{sort}','{keystr}','{valstr}')")

@router.get("/list_nopage") #read
def user_list():
    user = session.execute(f"call user_list_nopage()").all()
    return user

@router.post("/update") #update
def user_update(user: class_user.User_update):
    session.execute(f"call user_update('{user.user_id}','{user.user_name}','{user.user_email}','{user.user_password}','{user.user_phone}','{user.user_postcode}','{user.user_address}','{user.user_detailAddress}','{user.user_role}')")

@router.post("/delete") #delete
def user_delete(user: class_user.User_token):
    chk = session.execute(f"call user_delete('{user.user_token}','{user.user_id}')").all()
    return chk

@router.get("/readtoken/{token}") #read detail whom self
def user_read_token(token: str):
  userinfo = session.execute(f"call user_read_token('{token}')").all()
  return userinfo

@router.post("/readid") #read detail whom self
def user_read_id(user: class_user.User_token):
  userinfo = session.execute(f"call user_read_id('{user.user_id}')").all()
  return userinfo

@router.post("/user_data")
def user_data(user: class_user.user_data):
    result = session.execute(f"call user_data('{user.user_id}')").all()
    return result