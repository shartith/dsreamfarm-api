import sys
import socket
import json
from _thread import *

# 서버 정보
HOST = '52.79.223.31'
PORT = 22331

# 소켓서버 연결
data = "DEADBEEFFE01"

client_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
client_socket.connect((HOST, PORT))
client_socket.send(data.encode())

print('********************')
print('*  Connect Server  *')
print('********************')

try:
    while True:
        data = client_socket.recv(1024)

        if not data:
            break

        message = repr(data.decode())

        if message == 'Fail':
            break
        else:
            print(">> recive : ", message)
        
except Exception as e:
    print('>> Error : ', e)
finally:
    print('***********************')
    print('*  Disconnect Server  *')
    print('***********************')
    client_socket.close()
    sys.exit()