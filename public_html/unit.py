from fastapi import APIRouter, FastAPI
from pydantic import BaseModel
from typing import Optional,List

from DATABASE.db_conn import engineconn

router = APIRouter(
    prefix="/unit",  # url 앞에 고정적으로 붙는 경로추가
    tags=["unit"]
)  # Route 분리

app = FastAPI()

engine = engineconn()
session = engine.sessionmaker()

class Unit_data(BaseModel):
    controller_id: str

class Unit_group_change_auto_status(BaseModel):
    group_controller_id: str
    group_id: str
    group_auto_status: str

class Unit_change_auto_status(BaseModel):
    unit_controller_id: str
    unit_id: str
    unit_operation_type: str

class unit_change_current_status(BaseModel):
    unit_controller_id: str
    unit_id: str
    unit_operation_type: str

class Unit_timer_data(BaseModel):
    unit_controller_id: str
    unit_id: str


class update_device_temp(BaseModel):
    controller_id_in:str
    time_in:str
    temp:int

class read_device_temp(BaseModel):
    controller_id_in:str

class update_device_timer(BaseModel):
      controller_id_in: str
      unit_id_in: str
      time1_in: str
      time2_in : str
      status : int

class read_device_timer(BaseModel):
      controller_id_in: str
      unit_id_in :str


@router.post("/unit_data")
async def unit_data(controller:Unit_data):
  try:
    unit_data = session.execute(f"call unit_data('{controller.controller_id}')").all()
    return unit_data
  except Exception :
      return {"error": "데이터를 가져오는 도중에 오류가 발생했습니다."}
@router.post("/unit_change_auto_status")
async def unit_change_auto_status(unit:Unit_change_auto_status):
  unit_result = session.execute(f"call unit_change_auto_status('{unit.unit_controller_id}','{unit.unit_id}','{unit.unit_operation_type}')").all()
  return unit_result

@router.post("/unit_change_current_status")
async def unit_change_auto_status(unit:unit_change_current_status):
  unit_result = session.execute(f"call unit_change_current_status('{unit.unit_controller_id}','{unit.unit_id}','{unit.unit_operation_type}')").all()
  return unit_result

@router.post("/unit_group_data")
async def unit_group_data(controller:Unit_data):
  try:
    unit_data = session.execute(f"call unit_group_data('{controller.controller_id}')").all()
    return unit_data
  except Exception :
    return {"error": "데이터를 가져오는 도중에 오류가 발생했습니다."}

@router.post("/unit_group_change_auto_status")
async def unit_group_change_auto_status(unitGroup:Unit_group_change_auto_status):
  unitGroup_result = session.execute(f"call unit_group_change_auto_status('{unitGroup.group_controller_id}','{unitGroup.group_id}','{unitGroup.group_auto_status}')").all()
  return unitGroup_result

@router.post("/timer_detail")
async def timer_detail(unit:Unit_timer_data):
  timer_data = session.execute(f"call unit_timer_detail('{unit.unit_controller_id}','{unit.unit_id}')").all()
  return timer_data

@router.post("/update_device_timer")
async def update_device_timer(unit: update_device_timer):
  timer_data=session.execute("CALL update_device_timer(:controller_id_in, :unit_id_in, :time1_in, :time2_in, :status)", {
      "controller_id_in": unit.controller_id_in,
      "unit_id_in": unit.unit_id_in,
      "time1_in": unit.time1_in,
      "time2_in": unit.time2_in,
      "status": unit.status,}).all()
  return timer_data

@router.post("/read_device_timer")
async def timer_add(unit: read_device_timer):
  try:
    timer_data = session.execute(f"CALL read_device_timer(:controller_id_in, :unit_id_in)", {
        "controller_id_in": unit.controller_id_in,
        "unit_id_in": unit.unit_id_in,
    }).all()
    return timer_data
  except Exception :
      return {"error": "데이터를 가져오는 도중에 오류가 발생했습니다."}
@router.post("/update_device_temp")
async def update_device_temp(unit: update_device_temp):
    session.execute("CALL update_device_temp(:controller_id_in, :time_in, :temp)", {
        "controller_id_in": unit.controller_id_in,
        "time_in": unit.time_in,
        "temp": unit.temp,
    }).all()


@router.post("/read_device_temp")
async def timer_add(unit:read_device_temp):
  try:
    timer_data = session.execute(f"call read_device_temp('{unit.controller_id_in}')").all()
    return timer_data
  except Exception :
     return {"error": "데이터를 가져오는 도중에 오류가 발생했습니다."}