from fastapi import APIRouter, FastAPI
from pydantic import BaseModel
from typing import Optional, List
import requests
import json
import time

from DATABASE.db_conn import engineconn

router = APIRouter(
    prefix="/sms",
    tags=["sms"]
)

app = FastAPI()

engine = engineconn()
session = engine.sessionmaker()

mass_send_url = 'https://apis.aligo.in/send_mass/'




@router.post("/send_sms")
def send_sms():
    while True:
        result = session.execute("CALL sms_data()").all()
        if len(result) >= 1:
            sms_data = {
                'key': '8f6absa8gmm2nvad8caroxgeipfbemq9',  # api key
                'userid': 'asj0352',  # 알리고 사이트 아이디
                'sender': '01034841507',  # 발신번호
                'cnt': len(result),  # 메세지 전송건수(번호, 메세지 매칭건수)
                'msg_type': 'SMS',  # 메세지 타입 (SMS, LMS, MMS)
                'testmode_yn': 'N'  # 테스트모드 적용 여부 Y/N
            }
            delete_data = []  # 삭제할 데이터를 저장할 리스트

            for i, row in enumerate(result):
                sms_data[f'rec_{i+1}'] = row.unit_tel
                sms_data[f'msg_{i+1}'] = row.unit_sms
                delete_data.append({'unit_tel': row.unit_tel, 'unit_sms': row.unit_sms})  # 삭제할 데이터 추가

            requests.post(mass_send_url, data=sms_data)
            delete_sms_data(delete_data)  # 삭제할 데이터를 프로시저로 전달하여 삭제

        time.sleep(10)  # 10초 대기

def delete_sms_data(data):
    for item in data:
        unit_tel = item['unit_tel']
        unit_sms = item['unit_sms']
        session.execute("CALL sms_delete(:unit_tel, :unit_sms)", {'unit_tel': unit_tel, 'unit_sms': unit_sms})
    session.commit()

@router.post("/danger_sms")
def send_sms():
    while True:
        session.execute("CALL danger_message()")
        session.commit()
        time.sleep(60)  # 1분 대기