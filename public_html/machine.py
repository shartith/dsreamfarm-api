from fastapi import APIRouter, FastAPI
from pydantic import BaseModel
from typing import Optional

from DATABASE.db_conn import engineconn

router = APIRouter(
    prefix="/machine",  # url 앞에 고정적으로 붙는 경로추가
    tags=["machine"]
)  # Route 분리

app = FastAPI()

engine = engineconn()
session = engine.sessionmaker()

class Machine_read(BaseModel):
    user_id: str

class Machine_create(BaseModel):
    user_token: Optional[str]=None
    #machine_id: Optional[str]=None
    machine_user_id: str
    machine_name: str
    machine_type: Optional[str]="1"
    machine_lat: Optional[float]=0
    machine_log: Optional[float]=0
    machine_postcode: str
    machine_address: str
    machine_detailAddress: str
    machine_pairing: Optional[str]=None

class Machine_update(BaseModel):
    user_token: Optional[str]=None
    machine_id: int
    machine_user_id: str
    machine_name: str
    machine_type: Optional[str]="1"
    machine_lat: Optional[float]=0
    machine_log: Optional[float]=0
    machine_postcode: str
    machine_address: str
    machine_detailAddress: Optional[str]=None
    machine_pairing: Optional[str]=None

class Machine_delete(BaseModel):
    user_token:Optional[str]=None
    machine_id: int

class Machine_info(BaseModel):
    machine_id: int

class Machine_change_status(BaseModel):
   machine_id: int
   machine_status: int

@router.post("/create")
def machine_create(machine: Machine_create):
  session.execute(f"call machine_create('{machine.user_token}','{machine.machine_user_id}','{machine.machine_name}','{machine.machine_type}','{machine.machine_lat}','{machine.machine_log}','{machine.machine_postcode}','{machine.machine_address}','{machine.machine_detailAddress}','{machine.machine_pairing}')")

@router.post("/list")
def machine_list(machine: Machine_read):
    try:
        machine_list = session.execute(f"call machine_list('{machine.user_id}')").all()
        return machine_list
    except Exception as e:
        return {"error": str(e)}

@router.post("/update")
def machine_update(machine: Machine_update):
  session.execute(f"call machine_update('{machine.user_token}','{machine.machine_id}','{machine.machine_user_id}','{machine.machine_name}','{machine.machine_type}','{machine.machine_lat}','{machine.machine_log}','{machine.machine_postcode}','{machine.machine_address}','{machine.machine_detailAddress}','{machine.machine_pairing}')")

@router.post("/delete")
def machine_delete(machine:Machine_delete):
  session.execute(f"call machine_delete('{machine.user_token}', '{machine.machine_id}')")

@router.post("/info")
def machine_info(machine: Machine_info):
    try:
        machine_info = session.execute(f"call machine_info('{machine.machine_id}')").all()
        if machine_info:
            return machine_info
        else:
            return {"error": "1데이터를 가져오는 도중에 오류가 발생했습니다."}
    except Exception as e:
        return {"error": str(e)}


@router.post("/change_status")
def machine_change_status(machine: Machine_change_status):
   machine_status = session.execute(f"call machine_change_status('{machine.machine_id}','{machine.machine_status}')").all()
   return machine_status