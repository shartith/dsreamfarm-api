import socket
import json

from _thread import *
from DATABASE.db_conn import engineconn

# 연결된 클라이언트 목록
client_sockets = []
socket_lists = []

# 데이터베이스 연결
engine = engineconn()
session = engine.sessionmaker()

# 소켓으로부터 맥어드래스 대기
def listen_message(client_socket, addr):
    while True:
        try:
            # data에 맥어드레스가 들어올때까지 기다림
            data = client_socket.recv(1024) 

            if not data:
                print('>> Disconnect From Client : ', addr[0], ' / ', addr[1])
                break
            
            macaddr = data.decode()

            global socket_lists
            socket_lists.append({'key':macaddr,'socket':client_socket})

            print((socket_lists))
            
            dict_a = {}
            dict_1 = {}
            dict_2 = {}
            dict_3 = {}
            i = 0
            j = 0
            k = 0

            unit_data_lists = session.execute(f"call info_unit_data('{macaddr}')").all()
            for row in unit_data_lists:
                dict_1[i] = {'unit_id':row[0],'unit_open_channel':row[1],'unit_close_channel':row[2],'unit_operation_type':row[3],'unit_move_time':row[4],'unit_stop_time':row[5]}
                i+=1

            #unit_time_lists = session.execute(f"call info_unit_timer('{macaddr}')").all()
            #for row in unit_time_lists:
            #    dict_2.update({'unit_id':row[0],'unit_on_clock':row[1],'unit_off_clock':row[2]})
            
            #sens_data_lists = session.execute(f"call info_sensor_data('{macaddr}')").all()
            #for row in sens_data_lists:
            #    dict_3.update({'sensor_id':row[0],'sensor_channel':row[1],'sensor_operation_type':row[2],'sensor_mult':row[3],'sensor_offset':row[4],'sensor_equation':row[5]})
            
            #dict_a.update({'unit_data':dict_1})
            #dict_a.update({'unit_time':dict_2})
            #dict_a.update({'sensor_data':dict_3})

            print(dict_1)

            #client_socket.send(jString.encode())
            
        except ConnectionResetError as e:
            print('>> Disconnect Form Server: ' + addr[0], ' / ', addr[1])
            break

    # 위 while이 종료되면 클라이언트 목록에서 삭제
    if client_socket in client_sockets :
        client_sockets.remove(client_socket)
        print('>> Connected Count : ', len(client_sockets))

    client_socket.close()

# 서버 정보
HOST = '0.0.0.0'
PORT = 22331

# 소켓서버 오픈
print('******************')
print('*  Server Start  *')
print('******************')
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind((HOST, PORT))
server_socket.listen()

try:
    while True:
        # 아래에서 연결 수락 대기
        client_socket, addr = server_socket.accept()
        client_sockets.append(client_socket)
        start_new_thread(listen_message,(client_socket, addr))
except Exception as e:
    print('Error : ',e)
finally:
    server_socket.close()
